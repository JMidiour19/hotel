package bf.isge.ic3.dto;

import bf.isge.ic3.model.User;

public class UserMapper {

	public UserMapper() {}
	
	static UserDTO toDTO(User user ) {
		
		UserDTO userDTO = new UserDTO();
		userDTO.setEmail(user.getEmail());
		userDTO.setUsername(user.getUsername());
		userDTO.setFirstName(user.getFirstName());
		userDTO.setCreated(user.getCreated());
		userDTO.setGender(user.getGender());
		
		return userDTO;
	}
}
