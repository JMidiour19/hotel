package bf.isge.ic3.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import bf.isge.ic3.model.Role;

@Repository
public interface RoleRepo extends JpaRepository<Role, Long> {

	Role findByName(String name);
	
	Role findBySubName(String subname);
}
