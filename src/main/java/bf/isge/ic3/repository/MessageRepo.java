package bf.isge.ic3.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import bf.isge.ic3.model.Message;

public interface MessageRepo extends JpaRepository<Message, Long> {

	List<Message> findAllByChatId(long id);
}
