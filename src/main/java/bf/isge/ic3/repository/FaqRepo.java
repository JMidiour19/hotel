package bf.isge.ic3.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import bf.isge.ic3.model.Faq;

public interface FaqRepo extends JpaRepository<Faq, Long> {
	
	
}
