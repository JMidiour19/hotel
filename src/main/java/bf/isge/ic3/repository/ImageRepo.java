package bf.isge.ic3.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import bf.isge.ic3.model.Image;

public interface ImageRepo extends JpaRepository<Image, Long>{
	
	Image findOneByUserId(long id);
	
	Image findOneById(long id);

}
