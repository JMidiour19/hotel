package bf.isge.ic3.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import bf.isge.ic3.model.Request;

public interface RequestRepo extends JpaRepository<Request, Long> {
	
	List<Request> findAllByChecked(boolean check);

}
