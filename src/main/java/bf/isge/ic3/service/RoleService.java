package bf.isge.ic3.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import bf.isge.ic3.model.Role;
import bf.isge.ic3.repository.RoleRepo;

@Service
public class RoleService {

	@Autowired
	private RoleRepo roleRepo;
	
	public List<Role> findAll(){
		return roleRepo.findAll();
	}
	
	public Role findBySubName(String subname) {
		return roleRepo.findBySubName(subname);
	}
}
